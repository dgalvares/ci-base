<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends MY_Model
{
    protected $primary_key = 'cpf';

    public function autenticar($cpf, $senha)
    {
        return $this->get_by(['cpf' => $cpf, 'senha' => $senha]);
    }
}