<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!-- saved from url=(0060)https://getbootstrap.com/docs/4.0/examples/navbar-top-fixed/ -->
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="<?= base_url('/img/favicon.ico') ?>">

    <title>Bootstrap 4</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?= base_url('/vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
</head>

<body class="bg-secondary">

<!-- Page Content -->
<div class="container">
    <div class="row mt-5">
        <div class="col col-md-4 mx-auto">
            <div class="card">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <?= validation_errors() ?>
                    <?= form_open('autenticacao') ?>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                            <?= form_input('cpf', '', 'class="form-control" placeholder="CPF" data-mask="000.000.000-00"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                            <?= form_password('senha', '', 'class="form-control" placeholder="Senha"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= form_submit('login', 'Entrar', 'class="btn btn-success btn-block"') ?>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="<?= base_url('/vendor/jquery/jquery.min.js') ?>"></script>

<!-- Bootstrap JS -->
<script src="<?= base_url('/js/popper.min.js') ?>"></script>
<script src="<?= base_url('/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?= base_url('/js/ie10-viewport-bug-workaround.js') ?>"></script>

<!-- jQuery-Mask-Plugin -->
<script src="<?= base_url('/vendor/jquery-mask-plugin/jquery.mask.min.js') ?>"></script>

<?php if (isset($javascript)): ?>
    <!-- JavaScript da página -->
    <?= $javascript ?>
<?php endif; ?>

</body>
</html>