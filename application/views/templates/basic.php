<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!-- saved from url=(0060)https://getbootstrap.com/docs/4.0/examples/navbar-top-fixed/ -->
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="<?= base_url('/img/favicon.ico')?>">

    <title>Bootstrap 4</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?= base_url('/vendor/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url('/css/style.css')?>" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a href="#" class="navbar-brand">Bootstrap 4</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto dropdown">
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="cadusuarios-dropdown" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-users"></i> Usuários
                </a>
                <div class="dropdown-menu" aria-labelledby="cadusuarios-dropdown">
                    <a class="dropdown-item" href="<?= base_url('cadusuarios') ?>">Usuários cadastrados</a>
                    <a class="dropdown-item" href="<?= base_url('cadusuarios/criar') ?>">Adicionar Usuário</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<!-- Page Content -->
<?= $content ?>

<!-- jQuery -->
<script src="<?= base_url('/vendor/jquery/jquery.min.js')?>"></script>

<!-- Bootstrap JS -->
<script src="<?= base_url('/js/popper.min.js')?>"></script>
<script src="<?= base_url('/vendor/bootstrap/js/bootstrap.min.js')?>"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?= base_url('/js/ie10-viewport-bug-workaround.js')?>"></script>

<!-- DataTables JavaScript -->
<script src="<?= base_url('/vendor/DataTables/datatables.min.js')?>"></script>

<!-- jQuery-Mask-Plugin -->
<script src="<?= base_url('/vendor/jquery-mask-plugin/jquery.mask.min.js')?>"></script>

<!-- JavaScript da página -->
<?php if (isset($javascript)) echo $javascript; ?>

</body>
</html>