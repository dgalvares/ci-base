<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Inicializando a DataTable -->
<script>
    $(document).ready(function () {
        $('#datatable').DataTable({
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Resultados por página _MENU_",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            processing: true,
            dom: "<'row'<'mr-auto'l><'ml-auto'f><'ml-auto'B>><'row'r><'row't><'row'<'mr-auto'i><'ml-auto'p>>",
            buttons: [
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text-o fa-fw"></i> CSV',
                    className: 'btn btn-outline-dark'
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o fa-fw"></i> Excel',
                    className: 'btn btn-outline-success'
                },
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o fa-fw"></i> PDF',
                    className: 'btn btn-outline-danger'
                }
            ]
        });
    });
</script>