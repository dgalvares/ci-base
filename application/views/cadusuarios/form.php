<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2><i class="fa fa-users"></i> Cadastro de Usuários</h2>
        </div>
        <!-- /.col-lg-10 -->
    </div>
    <!-- /.row -->

    <?php if ($action == 'editar' && is_null($usuario)): ?>
        <p class="alert alert-danger">
            Usuário inexistente. Volte ao <a href="<?= base_url('cadusuarios') ?>"> Cadastro de Usuários</a>
        </p>
    <?php else: ?>
        <div class="card">
            <div class="card-header">
                <?php if ($action == 'criar'): ?>
                    Novo usuário
                <?php else: ?>
                    Editar usuário
                <?php endif; ?>
            </div>
            <div class="card-body">
                <?= form_open('cadusuarios/' . $action) ?>
                <div class="form-group">
                    <label for="cpf">CPF</label>
                    <?= form_error('cpf') ?>
                    <?= form_input('cpf', isset($usuario->cpf) ? maskCPF($usuario->cpf) : set_value('cpf'), 'class="form-control" placeholder="000.000.000-00" data-mask="000.000.000-00"'); ?>
                </div>
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <?= form_error('nome') ?>
                    <?= form_input('nome', isset($usuario->nome) ? $usuario->nome : set_value('nome'), 'class="form-control" placeholder="Nome"') ?>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <?= form_error('email') ?>
                    <?= form_input('email', isset($usuario->email) ? $usuario->email : set_value('email'), 'class="form-control" placeholder="exemplo@email.com"') ?>
                </div>
                <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <?= form_input('telefone', isset($usuario->telefone) ? $usuario->telefone : set_value('telefone'), 'class="form-control" placeholder="(00) 00000-0000" data-mask="(00) 00000-0000"') ?>
                </div>
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    <button type="reset" class="btn btn-outline-secondary">Limpar</button>
                    <a href="<?= base_url('cadusuarios') ?>" class="btn btn-outline-secondary">Cancelar</a>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<!-- /.container-fluid -->