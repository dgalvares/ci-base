<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>
                <i class="fa fa-users"></i> Cadastro de Usuários
                <a href="<?= base_url('cadusuarios/criar') ?>" class="btn btn-outline-success"
                   title="Adicionar Usuário">
                    <i class="fa fa-plus"></i>
                </a>
            </h2>
        </div>
        <!-- /.col-lg-10 -->
    </div>
    <!-- /.row -->

    <?php if (empty($usuarios)): ?>
        <p class="alert alert-info">
            Nenum usuário cadastrado.
        </p>
    <?php else: ?>
        <div class="card">
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-hover table-striped" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>CPF</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach ($usuarios as $usuario): ?>
                        <tr>
                            <td><?= maskCPF($usuario->cpf) ?></td>
                            <td><?= $usuario->nome ?></td>
                            <td><?= $usuario->email ?></td>
                            <td><?= $usuario->telefone ?></td>
                            <td style="text-align: center">
                                <div class="btn-group btn-group-sm">
                                    <a href="<?= base_url('cadusuarios/editar/' . $usuario->cpf) ?>"
                                       class="btn btn-outline-info">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <button class="btn btn-outline-danger" data-toggle="modal"
                                            data-target="#modalExcluir<?= $usuario->cpf ?>">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <div class="modal fade" id="modalExcluir<?= $usuario->cpf ?>" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="Confirmação de exclusão" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h2>Excluir usuário</h2>
                                            </div>
                                            <div class="modal-body">
                                                <p>Deseja realmente excluir esse usuário?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <?= form_open('cadusuarios/excluir', 'class="form"', ['cpf' => $usuario->cpf]) ?>
                                                <?= form_submit('excluir', 'Sim', 'class="btn btn-success"') ?>
                                                <?= form_button('nao-excluir', 'Não', 'class="btn btn-danger" data-dismiss="modal"') ?>
                                                <?= form_close() ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>
<!-- /.container-fluid -->