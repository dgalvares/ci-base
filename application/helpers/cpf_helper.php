<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CPF mágico 000.000.000-00
 */
defined('MAGIC_CPF_0') OR define('MAGIC_CPF_0', '00000000000');

/**
 * CPF mágico 111.111.111-11
 */
defined('MAGIC_CPF_1') OR define('MAGIC_CPF_1', '11111111111');

if (!function_exists('cleanCPF')) {

    /**
     * Remove os pontos e o hífen usados na máscara do CPF.
     *
     * @param string $cpf CPF com pontos e hífen
     * @param bool $toNumber se TRUE, retorna o CPF como um int
     *
     * @return string|int CPF sem pontos e hífen
     */
    function cleanCPF($cpf, $toNumber = true)
    {
        if (strlen($cpf) == 14) {
            $cpf = preg_replace('/(\.|-)/', '', $cpf);
        }
        return $toNumber ? intval($cpf) : $cpf;
    }
}

if (!function_exists('validateCPF')) {

    /**
     * Realiza a verificação do CPF pelo cálculo Módulo 11.
     *
     * @param string $cpf CPF com ou sem máscara
     *
     * @return bool TRUE caso o CPF seja válido, caso contrário FALSE
     */
    function validateCPF($cpf)
    {
        $CI =& get_instance();
        $CI->load->library('form_validation');

        $cpf = cleanCPF($cpf, false);

        if (strlen($cpf) != 11 || isMagicCPF($cpf)) {
            $CI->form_validation->set_message('validateCPF', 'Deve ser informado um CPF válido do campo {field}.');
            return false;
        }

        $cpfi = strrev(substr($cpf, 0, 9));
        $v1 = 0;
        $v2 = 0;
        for ($i = 0; $i < strlen($cpfi); $i++) {
            $v1 += intval($cpfi[$i]) * (9 - ($i % 10));
            $v2 += intval($cpfi[$i]) * (9 - (($i + 1) % 10));
        }
        $v1 = ($v1 % 11) % 10;
        $v2 += $v1 * 9;
        $v2 = ($v2 % 11) % 10;

        if ($v1 == intval($cpf[9]) && $v2 == intval($cpf[10])) {
            return true;
        } else {
            $CI->form_validation->set_message('validateCPF', 'Deve ser informado um CPF válido do campo {field}.');
            return false;
        }
    }
}

if (!function_exists('isMagicCPF')) {

    /**
     * Verifica se o CPF é 000.000.000-00 ou 111.111.111-11.
     *
     * @param string $cpf
     *
     * @return bool
     */
    function isMagicCPF($cpf)
    {
        $cpf = cleanCPF($cpf);
        return $cpf == MAGIC_CPF_0 || $cpf == MAGIC_CPF_1;
    }
}

if (!function_exists('maskCPF')) {
    function maskCPF($cpf)
    {
        if (!is_string($cpf)) {
            $cpf = strval($cpf);
        }

        if (($len = strlen($cpf)) < 11) {
            $cpf = str_repeat('0', 11 - $len) . $cpf;
        }

        return substr($cpf, 0, 3) . '.' . substr($cpf, 3, 3) . '.' . substr($cpf, 6, 3) . '.' . substr($cpf, 9);
    }
}