<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('login')) {
    /**
     * Salva os dados do usuário logado na sessão
     *
     * @param mixed $userData dados do usuário logado, pode ser qualquer tipo de dado
     */
    function login($userData)
    {
        $ci =& get_instance();
        $ci->session->set_userdata('loggedUser', $userData);
    }
}

if (!function_exists('logout')) {
    /**
     * Destrói a sessão, inclusive do usuário logado
     */
    function logout()
    {
        session_destroy();
    }
}

if (!function_exists('loggedUser')) {
    /**
     * Acessa os dados do usuário logado na sessão
     *
     * @return mixed dados do usuário logado, NULL se não houverem dados
     */
    function loggedUser()
    {
        $ci =& get_instance();
        return $ci->session->userdata('loggedUser');
    }
}