<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['error_prefix'] = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>';
$config['error_suffix'] = '</div>';
