<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Realiza autenticação do usuário, login e logout
 *
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_Session $session
 *
 * @property Usuario_model $usuario_model
 */
class Autenticacao extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('usuario_model');
    }

    public function index()
    {
        $this->form_validation->set_rules('cpf', 'CPF', 'required|cleanCPF');

        if ($this->form_validation->run() === false) {
            $this->load->view('login-form');
        } else {
            $cpf = $this->input->post('cpf');
            $senha = $this->input->post('senha');
            $usuario = $this->usuario_model->autenticar($cpf, $senha);
            if (isset($usuario)) {
                login($usuario);
                redirect('cadusuarios');
            } else {
                $this->form_validation->set_message('cpf', 'CPF ou Senha inválidos');
                $this->load->view('login-form');
            }
        }
    }
}
