<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class CRUD Usuarios.
 *
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_Session $session
 *
 * @property Usuario_model $usuario_model
 */
class Cadusuarios extends CI_Controller
{
    /**
     * @var array regras de validação
     */
    private $rules = array(
        'cpf' => ['CPF', 'cleanCPF|required|is_unique[usuario.cpf]|validateCPF'],
        'nome' => ['Nome', 'trim|required'],
        'email' => ['E-mail', 'trim|valid_email'],
        'telefone' => ['Telefone', 'trim']
    );

    /**
     * Cadusuarios constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('usuario_model');
    }

    /**
     * Listagem de usuários.
     */
    public function index()
    {
        // dados carregados para a view
        $data = array(

            // lista de usuários
            'usuarios' => $this->usuario_model->get_all(),

            // javascript específico para esta página
            'javascript' => $this->load->view('datatables-conf-js', '', true)
        );

        // view a ser carregada
        $data['content'] = $this->load->view('cadusuarios/lista', $data, true);

        // template utilizado
        $this->load->view('templates/basic', $data);
    }

    /**
     * Cria um novo usuário.
     */
    public function criar()
    {
        // regras de validação
        $this->set_rules();

        // verifica se a validação foi realizada
        // ou se houve erro
        if ($this->form_validation->run() === false) {

            // dados carregados para a view
            $data = array(

                // action sendo executada
                'action' => 'criar'
            );

            // view a ser carregada
            $data['content'] = $this->load->view('cadusuarios/form', $data, true);

            // template utilizado
            $this->load->view('templates/basic', $data);

        } else {
            // se a validação foi um sucesso
            // adiciona adiciona o novo usuário no banco de dados
            $this->usuario_model->insert($this->input->post());

            // redireciona para a lista de usuários
            redirect('cadusuarios');
        }
    }

    /**
     * Edita um usuário existente.
     *
     * @param string $cpf CPF do usuário editado
     */
    public function editar($cpf = null)
    {
        // verifica se o CPF foi passado por GET ou POST
        if (isset($cpf)) {
            // caso passado por GET

            // recupera os dados do usuário no banco de dados
            // e os salva na seção
            $this->session->set_userdata('usuario_editado', $this->usuario_model->get(cleanCPF($cpf)));

        } else {
            // caso passado por POST

            // recupera os dados do usuário no formulário
            $usuario = $this->input->post();

            // limpa os dados do formulário
            $usuario['cpf'] = cleanCPF($usuario['cpf']);
            $usuario['nome'] = trim($usuario['nome']);
            $usuario['email'] = trim($usuario['email']);

            // mantém no POST apenas os dados alterados
            $_POST = array_diff_assoc($usuario, (array)$this->session->usuario_editado);

            // se não sobrarem alterações
            if (empty($this->input->post())) {
                redirect('cadusuarios');
            }
        }

        // regras de validação
        $this->set_rules($this->input->post());

        // verifica se houve validação
        // ou se houve erro na validação
        if ($this->form_validation->run() === false) {

            // dados carregados para a view
            $data = array(

                // action sendo executada
                'action' => 'editar',

                // dados do usuário
                'usuario' => $this->session->usuario_editado
            );

            // view carregada
            $data['content'] = $this->load->view('cadusuarios/form', $data, true);

            // template utilizado
            $this->load->view('templates/basic', $data);

        } else {
            // se a validação foi um sucesso
            // altera os dados do usuário
            $this->usuario_model->update($this->session->usuario_editado->cpf, $this->input->post());

            // redireciona para a lista de usuários
            redirect('cadusuarios');
        }
    }

    /**
     * Exclui um usuário.
     */
    public function excluir()
    {
        // verifica se o CPF foi passado por POST
        if ($this->input->post('cpf')) {

            // aplica soft delete ao usuário
            $this->usuario_model->delete($this->input->post('cpf'));

            // redireciona para a lista de usuários
            redirect('cadusuarios');

        } else {
            // caso seja tentado acesso direto à URL
            // mostra erro de página não encontrada
            show_404();
        }
    }

    /**
     * Define as regras de validação que serão usadas com base na entrada
     *
     * @param array $input array com as chaves das regras a serem utilizadas
     */
    private function set_rules($input = null)
    {
        if (isset($input)) {
            foreach (array_intersect_key($this->rules, $this->input->post()) as $key => $rule) {
                $this->form_validation->set_rules($key, $rule[0], $rule[1]);
            }
        } else {
            foreach ($this->rules as $key => $rule) {
                $this->form_validation->set_rules($key, $rule[0], $rule[1]);
            }
        }
    }
}
