CREATE TABLE "abstract_table" (
  "created_at" TIMESTAMP NOT NULL DEFAULT current_timestamp,
  "created_by" BIGINT,
  "updated_at" TIMESTAMP NOT NULL DEFAULT current_timestamp,
  "updated_by" BIGINT,
  "deleted"    BOOLEAN   NOT NULL DEFAULT FALSE,
  "log"        JSONB
);

CREATE TABLE "usuario" (
  "cpf"      BIGINT                 NOT NULL PRIMARY KEY,
  "nome"     CHARACTER VARYING(150) NOT NULL,
  "email"    CHARACTER VARYING(128),
  "telefone" CHARACTER VARYING(15),
  "senha"    CHARACTER VARYING(128)
)
  INHERITS ("abstract_table");

ALTER TABLE "usuario"
  ADD CONSTRAINT "usuario_fk_created_by" FOREIGN KEY ("created_by") REFERENCES "usuario" ("cpf");
ALTER TABLE "usuario"
  ADD CONSTRAINT "usuario_fk_updated_by" FOREIGN KEY ("updated_by") REFERENCES "usuario" ("cpf");